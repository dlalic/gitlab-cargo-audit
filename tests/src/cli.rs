extern crate assert_cmd;
extern crate predicates;
extern crate tempfile;

mod json {
    use assert_cmd::Command;
    use predicates::path;
    use predicates::prelude::*;
    use predicates::str;
    use std::fs;
    use std::path::Path;
    use tempfile::tempdir;

    #[test]
    fn valid_stdin() {
        let mut cmd = Command::cargo_bin("gitlab-cargo-audit").expect("Can not create command");
        cmd.pipe_stdin("tests/fixtures/example.json")
            .expect("Can not open input file");

        let expected = fs::read_to_string("tests/fixtures/output-from-json.json")
            .expect("Can not load fixture");
        cmd.arg("json")
            .assert()
            .success()
            .stderr(str::is_empty())
            .stdout(expected);
    }

    #[test]
    fn valid_file() {
        let mut cmd = Command::cargo_bin("gitlab-cargo-audit").expect("Can not create command");

        let expected = fs::read_to_string("tests/fixtures/output-from-json.json")
            .expect("Can not load fixture");
        cmd.arg("json")
            .arg("-f")
            .arg("tests/fixtures/example.json")
            .assert()
            .success()
            .stderr(str::is_empty())
            .stdout(expected);
    }

    #[test]
    fn invalid_file() {
        let mut cmd = Command::cargo_bin("gitlab-cargo-audit").expect("Can not create command");

        cmd.arg("json")
            .arg("-f")
            .arg("tests/fixtures/example.lock")
            .assert()
            .failure()
            .stderr(str::contains("Error"))
            .stdout(str::is_empty());
    }

    #[test]
    fn valid_stdin_to_output() {
        let dir = tempdir().expect("Can not create temporary dir");
        let output = dir.path().join("output.json");
        assert!(path::is_file().not().eval(&output));

        let mut cmd = Command::cargo_bin("gitlab-cargo-audit").expect("Can not create command");
        cmd.pipe_stdin("tests/fixtures/example.json")
            .expect("Can not open input file");

        cmd.arg("-o")
            .arg(&output)
            .arg("json")
            .assert()
            .success()
            .stderr(str::is_empty())
            .stdout(str::is_empty());

        let predicate_file =
            predicate::path::eq_file(Path::new("tests/fixtures/output-from-json.json"));
        assert!(predicate_file.eval(Path::new(&output)));

        drop(output);
        dir.close().expect("Can not close temporary dir");
    }
}
