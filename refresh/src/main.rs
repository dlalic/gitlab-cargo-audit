use reqwest::get;
use schemafy_lib::Generator;
use tokio::fs::write;

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let target = "https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/master/dist/sast-report-format.json";
    let response = get(target).await?;
    let body = response.text().await?;
    write("../schema.json", body).await?;

    Generator::builder()
        .with_root_name_str("Schema")
        .with_input_file("../schema.json")
        .build()
        .generate_to_file("../schema.rs")?;
    Ok(())
}
