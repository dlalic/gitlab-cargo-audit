use anyhow::Context;
use clap::Parser;
use gitlab_cargo_audit::audit::cli::Cli;
use gitlab_cargo_audit::audit::schema::Schema;
use std::fs::File;
use std::io::{BufWriter, Write};

fn main() -> Result<(), anyhow::Error> {
    let cli = Cli::parse();
    let write = match cli.output.as_deref() {
        Some(path) => Box::new(File::create(path)?) as Box<dyn Write>,
        None => Box::new(std::io::stdout()) as Box<dyn Write>,
    };
    let writer = BufWriter::new(write);
    let schema = Schema::try_from(cli.command)?;
    serde_json::to_writer_pretty(writer, &schema).context("Can not serialize report")
}
