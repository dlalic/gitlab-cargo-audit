use crate::audit::schema::{
    Schema, SchemaItemVulnerabilities, SchemaItemVulnerabilitiesItemLinks,
    SchemaItemVulnerabilitiesLocation, SchemaScan, SchemaScanAnalyzer, SchemaScanAnalyzerVendor,
    SchemaScanScanner, SchemaScanScannerVendor,
};
use rustsec::{Report, Vulnerability};

impl From<Report> for Schema {
    fn from(report: Report) -> Self {
        let vulnerabilities = match report.vulnerabilities.found {
            true => report
                .vulnerabilities
                .list
                .iter()
                .filter(|vulnerability| !vulnerability.versions.patched().is_empty())
                .map(|vulnerability| SchemaItemVulnerabilities::from(vulnerability.clone()))
                .collect::<Vec<_>>(),
            false => vec![],
        };
        Schema {
            remediations: None,
            scan: SchemaScan {
                analyzer: SchemaScanAnalyzer {
                    id: "cargo-audit".to_string(),
                    name: "".to_string(),
                    url: None,
                    vendor: SchemaScanAnalyzerVendor {
                        name: "".to_string(),
                    },
                    version: "".to_string(),
                },
                end_time: "".to_string(),
                messages: None,
                primary_identifiers: None,
                scanner: SchemaScanScanner {
                    id: "".to_string(),
                    name: "".to_string(),
                    url: None,
                    vendor: SchemaScanScannerVendor {
                        name: "".to_string(),
                    },
                    version: "".to_string(),
                },
                start_time: "".to_string(),
                status: "".to_string(),
                type_: "".to_string(),
            },
            schema: None,
            version: "".to_string(),
            vulnerabilities,
        }
    }
}

impl From<Vulnerability> for SchemaItemVulnerabilities {
    fn from(vulnerability: Vulnerability) -> Self {
        SchemaItemVulnerabilities {
            description: Option::from(vulnerability.advisory.description),
            flags: None,
            id: vulnerability.advisory.id.to_string(),
            identifiers: vec![],
            links: vulnerability.advisory.url.map(|v| {
                vec![SchemaItemVulnerabilitiesItemLinks {
                    name: None,
                    url: v.to_string(),
                }]
            }),
            location: SchemaItemVulnerabilitiesLocation {
                class: None,
                end_line: None,
                file: None,
                method: None,
                start_line: None,
            },
            name: Option::from(vulnerability.advisory.title),
            raw_source_code_extract: None,
            severity: Some("Low".to_string()), // TODO:
            solution: None,
            tracking: None,
        }
    }
}
