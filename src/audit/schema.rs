use serde::{Deserialize, Serialize};

#[doc = " A codeblock"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "code")]
pub struct Code {
    #[doc = " A programming language"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lang: Option<String>,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
    pub value: String,
}
#[doc = " A commit/tag/branch within the GitLab project"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "commit")]
pub struct Commit {
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
    #[doc = " The commit SHA"]
    pub value: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum DetailTypeValue {
    Variant0(NamedList),
    Variant1(List),
    Variant2(Table),
    Variant3(Text),
    Variant4(Url),
    Variant5(Code),
    Variant6(Value),
    Variant7(Diff),
    Variant8(Markdown),
    Variant9(Commit),
    Variant10(FileLocation),
    Variant11(ModuleLocation),
}
pub type DetailType = DetailTypeValue;
#[doc = " A diff"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "diff")]
pub struct Diff {
    pub after: String,
    pub before: String,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[doc = " A location within a file in the project"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "file_location")]
pub struct FileLocation {
    pub file_name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub line_end: Option<i64>,
    pub line_start: i64,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[doc = " A list of typed fields"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "list")]
pub struct List {
    pub items: Vec<DetailType>,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[doc = " GitLab flavoured markdown, see https://docs.gitlab.com/ee/user/markdown.html"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "markdown")]
pub struct Markdown {
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
    pub value: TextValue,
}
#[doc = " A location within a binary module of the form module+relative_offset"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "module_location")]
pub struct ModuleLocation {
    pub module_name: String,
    pub offset: i64,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "named_field")]
pub struct NamedField {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<TextValue>,
    pub name: TextValue,
}
#[doc = " An object with named and typed fields"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "named_list")]
pub struct NamedList {
    pub items: ::std::collections::BTreeMap<String, serde_json::Value>,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[doc = " A table of typed fields"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "table")]
pub struct Table {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub header: Option<Vec<DetailType>>,
    pub rows: Vec<Vec<DetailType>>,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[doc = " Raw text"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "text")]
pub struct Text {
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
    pub value: TextValue,
}
pub type TextValue = String;
#[doc = " A single URL"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "url")]
pub struct Url {
    pub href: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub text: Option<TextValue>,
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
}
#[doc = " A field that can store a range of types of value"]
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "value")]
pub struct Value {
    #[serde(rename = "type")]
    pub type_: serde_json::Value,
    pub value: serde_json::Value,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaItemRemediationsItemFixes {
    #[doc = " Unique identifier of the vulnerability. This is recommended to be a UUID."]
    pub id: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaItemRemediations {
    #[doc = " A base64-encoded remediation code diff, compatible with git apply."]
    pub diff: String,
    #[doc = " An array of strings that represent references to vulnerabilities fixed by this remediation."]
    pub fixes: Vec<SchemaItemRemediationsItemFixes>,
    #[doc = " An overview of how the vulnerabilities were fixed."]
    pub summary: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScanAnalyzerVendor {
    #[doc = " The name of the vendor."]
    pub name: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScanAnalyzer {
    #[doc = " Unique id that identifies the analyzer."]
    pub id: String,
    #[doc = " A human readable value that identifies the analyzer, not required to be unique."]
    pub name: String,
    #[doc = " A link to more information about the analyzer."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[doc = " The vendor/maintainer of the analyzer."]
    pub vendor: SchemaScanAnalyzerVendor,
    #[doc = " The version of the analyzer."]
    pub version: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScanItemMessages {
    #[doc = " Describes the severity of the communication. Use info to communicate normal scan behaviour; "]
    #[doc = " warn to communicate a potentially recoverable problem, or a partial error; fatal to "]
    #[doc = " communicate an issue that causes the scan to halt."]
    pub level: String,
    #[doc = " The message to communicate."]
    pub value: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScanItemPrimaryIdentifiers {
    #[doc = " Human-readable name of the identifier."]
    pub name: String,
    #[doc = " for example, cve, cwe, osvdb, usn, or an analyzer-dependent type such as gemnasium)."]
    #[serde(rename = "type")]
    pub type_: String,
    #[doc = " URL of the identifier's documentation."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[doc = " Value of the identifier, for matching purpose."]
    pub value: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScanScannerVendor {
    #[doc = " The name of the vendor."]
    pub name: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScanScanner {
    #[doc = " Unique id that identifies the scanner."]
    pub id: String,
    #[doc = " A human readable value that identifies the scanner, not required to be unique."]
    pub name: String,
    #[doc = " A link to more information about the scanner."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[doc = " The vendor/maintainer of the scanner."]
    pub vendor: SchemaScanScannerVendor,
    #[doc = " The version of the scanner."]
    pub version: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaScan {
    #[doc = " Object defining the analyzer used to perform the scan. Analyzers typically delegate to an "]
    #[doc = " underlying scanner to run the scan."]
    pub analyzer: SchemaScanAnalyzer,
    #[doc = " ISO8601 UTC value with format yyyy-mm-ddThh:mm:ss, representing when the scan finished."]
    pub end_time: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub messages: Option<Vec<SchemaScanItemMessages>>,
    #[doc = " An unordered array containing an exhaustive list of primary identifiers for which the "]
    #[doc = " analyzer may return results"]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub primary_identifiers: Option<Vec<SchemaScanItemPrimaryIdentifiers>>,
    #[doc = " Object defining the scanner used to perform the scan."]
    pub scanner: SchemaScanScanner,
    #[doc = " ISO8601 UTC value with format yyyy-mm-ddThh:mm:ss, representing when the scan started."]
    pub start_time: String,
    #[doc = " Result of the scan."]
    pub status: String,
    #[doc = " Type of the scan."]
    #[serde(rename = "type")]
    pub type_: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaItemVulnerabilitiesItemFlags {
    #[doc = " What the flag is about."]
    pub description: String,
    #[doc = " Tool that issued the flag."]
    pub origin: String,
    #[doc = " Result of the scan."]
    #[serde(rename = "type")]
    pub type_: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaItemVulnerabilitiesItemIdentifiers {
    #[doc = " Human-readable name of the identifier."]
    pub name: String,
    #[doc = " for example, cve, cwe, osvdb, usn, or an analyzer-dependent type such as gemnasium)."]
    #[serde(rename = "type")]
    pub type_: String,
    #[doc = " URL of the identifier's documentation."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[doc = " Value of the identifier, for matching purpose."]
    pub value: String,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaItemVulnerabilitiesItemLinks {
    #[doc = " Name of the vulnerability details link."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[doc = " URL of the vulnerability details document."]
    pub url: String,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SchemaItemVulnerabilitiesLocation {
    #[doc = " Provides the name of the class where the vulnerability is located."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub class: Option<String>,
    #[doc = " The last line of the code affected by the vulnerability."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub end_line: Option<f64>,
    #[doc = " Path to the file where the vulnerability is located."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub file: Option<String>,
    #[doc = " Provides the name of the method where the vulnerability is located."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[doc = " The first line of the code affected by the vulnerability."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub start_line: Option<f64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SchemaItemVulnerabilitiesTracking {
    #[doc = " Each tracking type must declare its own type."]
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct SchemaItemVulnerabilities {
    #[doc = " A long text section describing the vulnerability more fully."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    //#[serde(skip_serializing_if = "Option::is_none")]
    //pub details: Option<Items>,
    #[doc = " Flags that can be attached to vulnerabilities."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub flags: Option<Vec<SchemaItemVulnerabilitiesItemFlags>>,
    #[doc = " Unique identifier of the vulnerability. This is recommended to be a UUID."]
    pub id: String,
    #[doc = " An ordered array of references that identify a vulnerability on internal or external "]
    #[doc = " databases. The first identifier is the Primary Identifier, which has special meaning."]
    pub identifiers: Vec<SchemaItemVulnerabilitiesItemIdentifiers>,
    #[doc = " An array of references to external documentation or articles that describe the "]
    #[doc = " vulnerability."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub links: Option<Vec<SchemaItemVulnerabilitiesItemLinks>>,
    #[doc = " Identifies the vulnerability's location."]
    pub location: SchemaItemVulnerabilitiesLocation,
    #[doc = " The name of the vulnerability. This must not include the finding's specific information."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[doc = " Provides an unsanitized excerpt of the affected source code."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub raw_source_code_extract: Option<String>,
    #[doc = " How much the vulnerability impacts the software. Possible values are Info, Unknown, Low, "]
    #[doc = " Medium, High, or Critical. Note that some analyzers may not report all these possible "]
    #[doc = " values."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub severity: Option<String>,
    #[doc = " Explanation of how to fix the vulnerability."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub solution: Option<String>,
    #[doc = " Describes how this vulnerability should be tracked as the project changes."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tracking: Option<SchemaItemVulnerabilitiesTracking>,
}

#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
pub struct Schema {
    #[doc = " An array of objects containing information on available remediations, along with patch "]
    #[doc = " diffs to apply."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub remediations: Option<Vec<SchemaItemRemediations>>,
    pub scan: SchemaScan,
    #[doc = " URI pointing to the validating security report schema."]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schema: Option<String>,
    #[doc = " The version of the schema to which the JSON report conforms."]
    pub version: String,
    #[doc = " Array of vulnerability objects."]
    pub vulnerabilities: Vec<SchemaItemVulnerabilities>,
}
