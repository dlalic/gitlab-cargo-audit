use crate::audit::cli::Commands;
use crate::audit::schema::Schema;
use anyhow::{anyhow, Context};
use cargo_metadata::MetadataCommand;
use rustsec::{Lockfile, Report};
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;

impl TryFrom<Commands> for Schema {
    type Error = anyhow::Error;

    fn try_from(value: Commands) -> Result<Self, Self::Error> {
        match value {
            Commands::Lockfile { file } => {
                let cargo_lock = match file {
                    Some(path) => path,
                    None => {
                        let path = Path::new("Cargo.lock");
                        if !path.exists() && Path::new("Cargo.toml").exists() {
                            return Err(anyhow!(
                        "Can not find lock file, generate one with 'cargo generate-lockfile'"
                    ));
                        }
                        path.to_path_buf()
                    }
                };

                let lockfile = Lockfile::load(cargo_lock).context("Can not load lockfile")?;
                let report = fetch_report(&lockfile).context("Can not fetch report")?;
                let mut schema = Schema::from(report);

                let metadata = MetadataCommand::new()
                    .manifest_path("./Cargo.toml")
                    .exec()
                    .context("Can not load ./Cargo.toml")?;
                let info = metadata.packages.iter().find(|v| v.name == "rustsec");

                if let Some(package) = info {
                    schema.scan.scanner.name = package.name.to_string();
                    schema.scan.scanner.version = package.version.to_string();
                }

                Ok(schema)
            }
            Commands::Json { file } => {
                let read = match file {
                    Some(path) => Box::new(File::open(path)?) as Box<dyn Read>,
                    None => Box::new(std::io::stdin()) as Box<dyn Read>,
                };

                let reader = BufReader::new(read);
                let report: Report = serde_json::from_reader(reader)?;
                let schema = Schema::from(report);
                Ok(schema)
            }
        }
    }
}

#[cfg(not(test))]
fn fetch_report(lockfile: &Lockfile) -> Result<Report, anyhow::Error> {
    use rustsec::report::Settings;
    use rustsec::repository::git::DEFAULT_URL;
    use rustsec::Repository;

    let advisory_db_path = Repository::default_path();
    let advisory_db_repo = Repository::fetch(DEFAULT_URL, advisory_db_path, true)?;
    let database = rustsec::Database::load_from_repo(&advisory_db_repo)?;
    let report_settings = Settings::default();
    let report = Report::generate(&database, lockfile, &report_settings);
    Ok(report)
}

#[cfg(test)]
fn fetch_report(_: &Lockfile) -> Result<Report, anyhow::Error> {
    let file = File::open("tests/fixtures/example.json")?;
    let reader = BufReader::new(file);
    let report: Report = serde_json::from_reader(reader)?;
    Ok(report)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    #[test]
    fn valid_lockfile() {
        let fixture = PathBuf::from(r"tests/fixtures/example.lock");
        let result = Schema::try_from(Commands::Lockfile {
            file: Some(fixture),
        })
        .expect("Can not generate report");
        assert_eq!(result.vulnerabilities.len(), 2);
    }

    #[test]
    fn invalid_lockfile() {
        let fixture = PathBuf::from(r"tests/fixtures/example.json");
        let result = Schema::try_from(Commands::Lockfile {
            file: Some(fixture),
        });
        assert!(result.is_err());
        assert_eq!(
            result.err().expect("error").to_string(),
            "Can not load lockfile"
        );
    }

    #[test]
    fn no_lockfile() {
        let result =
            Schema::try_from(Commands::Lockfile { file: None }).expect("Can not generate report");
        assert_eq!(result.vulnerabilities.len(), 2);
    }

    #[test]
    fn valid_json() {
        let fixture = PathBuf::from(r"tests/fixtures/example.json");
        let result = Schema::try_from(Commands::Json {
            file: Some(fixture),
        })
        .expect("Can not generate report");
        assert_eq!(result.vulnerabilities.len(), 2);
    }

    #[test]
    fn invalid_json() {
        let fixture = PathBuf::from(r"tests/fixtures/example.lock");
        let result = Schema::try_from(Commands::Json {
            file: Some(fixture),
        });
        assert!(result.is_err());
        assert_eq!(
            result.err().expect("error").to_string(),
            "expected value at line 1 column 1"
        );
    }
}
