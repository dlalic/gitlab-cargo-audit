# GitLab cargo audit

[![pipeline status](https://gitlab.com/dlalic/gitlab-cargo-audit/badges/master/pipeline.svg)](https://gitlab.com/dlalic/gitlab-cargo-audit/-/commits/master)
[![coverage report](https://gitlab.com/dlalic/gitlab-cargo-audit/badges/master/coverage.svg)](https://gitlab.com/dlalic/gitlab-cargo-audit/-/commits/master)

Convert cargo audit warnings into GitLab SAST report

## Usage

### GitLab CI example

```
sast:
  stage: test
  before_script:
    - cargo install gitlab_cargo_audit
  script:
    - $CARGO_HOME/bin/gitlab-cargo-audit -o gl-sast-report.json lockfile
  artifacts:
    reports:
      sast: gl-sast-report.json
  rules:
    - if: '$SAST's
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'
```
